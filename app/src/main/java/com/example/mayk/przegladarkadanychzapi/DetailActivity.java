package com.example.mayk.przegladarkadanychzapi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {


    TextView firstNameTxt, lastNameTxt, phoneTxt, emailTxt;
    WebView photoImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        firstNameTxt = (TextView) findViewById(R.id.firstNameDetailTxt);
        lastNameTxt= (TextView) findViewById(R.id.lastNameDetailTxt);
        phoneTxt = (TextView) findViewById(R.id.phoneDetailTxt);
        emailTxt= (TextView) findViewById(R.id.emailDetailTxt);
        photoImg = (WebView) findViewById(R.id.articleDetailImg);

       //GET INTENT
        Intent i=this.getIntent();

        //RECEIVE DATA
        String firstName=i.getExtras().getString("FIRST_NAME_KEY");
        String lastName=i.getExtras().getString("LAST_NAME_KEY");
        String email=i.getExtras().getString("EMAIL_KEY");
        String phone=i.getExtras().getString("PHONE_KEY");
        String photo=i.getExtras().getString("PHOTO_KEY");

        //BIND DATA
        firstNameTxt.setText(firstName);
        lastNameTxt.setText(lastName);
        phoneTxt.setText(phone);
        emailTxt.setText(email);
        photoImg.loadUrl(photo);


      }

   }
