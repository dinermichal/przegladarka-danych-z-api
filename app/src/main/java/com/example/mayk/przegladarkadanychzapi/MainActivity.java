package com.example.mayk.przegladarkadanychzapi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.widget.ListView;

import com.example.mayk.przegladarkadanychzapi.m_JASON.JSONDownloader;



public class MainActivity extends AppCompatActivity {

    String jsonURL="https://randomuser.me/api/?inc=gender,name,nat,phone,email,picture&results=10";
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.lv);

        new JSONDownloader(MainActivity.this, jsonURL, lv).execute();

    }
}



