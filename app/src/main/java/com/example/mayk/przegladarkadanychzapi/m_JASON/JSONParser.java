package com.example.mayk.przegladarkadanychzapi.m_JASON;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;


import com.example.mayk.przegladarkadanychzapi.m_Model.User;
import com.example.mayk.przegladarkadanychzapi.m_UI.CustomAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONParser extends AsyncTask<Void,Void,Boolean>{

    private Context c;
    private String jsonData;
    private ListView lv;

    private ProgressDialog pd;
    ArrayList<User> users=new ArrayList<>();

    public JSONParser(Context c, String jsonData, ListView lv) {
        this.c = c;
        this.jsonData = jsonData;
        this.lv = lv;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd=new ProgressDialog(c);
        pd.setTitle("Parse");
        pd.setMessage("Parsing...Please wait");
        pd.show();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        return parse();
    }

    @Override
    protected void onPostExecute(Boolean isParsed) {
        super.onPostExecute(isParsed);

        pd.dismiss();
        if(isParsed)
        {
            //BIND
            lv.setAdapter(new CustomAdapter(c,users));
        }else
        {
            Toast.makeText(c, "Unable To Parse,Check Your Log output", Toast.LENGTH_SHORT).show();
        }

    }

    private Boolean parse()
    {
        try
        {
           // JSONArray jsonarray=new JSONArray(jsonData);

            users.clear();
            User user;

            JSONObject baseJsonResponse = new JSONObject(jsonData);

            JSONArray peopleArray = baseJsonResponse.getJSONArray("results");

            for (int i=0;i<peopleArray.length();i++)
            {
                JSONObject currentPerson = peopleArray.getJSONObject(i);
                JSONObject detailedNames = currentPerson.getJSONObject("name");
                JSONObject picture = currentPerson.getJSONObject("picture");
                String firstName =detailedNames.getString("first");
                String lastName =detailedNames.getString("last");
                String email = currentPerson.getString("email");
                String phone =currentPerson.getString("phone");
                String photo =picture.getString("large");

                user=new User();

                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setPhone(phone);
                user.setPhoto(photo);

                users.add(user);
            }

            return true;

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }


}



















