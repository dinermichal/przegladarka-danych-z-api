package com.example.mayk.przegladarkadanychzapi.m_UI;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;


import com.example.mayk.przegladarkadanychzapi.DetailActivity;
import com.example.mayk.przegladarkadanychzapi.R;
import com.example.mayk.przegladarkadanychzapi.m_Model.User;

import java.util.ArrayList;


public class CustomAdapter  extends BaseAdapter{

    Context c;
    ArrayList<User> users;
    EditText search;

    public CustomAdapter(Context c, ArrayList<User> users) {
        this.c = c;
        this.users = users;
    }




    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null)
        {
            view=LayoutInflater.from(c).inflate(R.layout.model,viewGroup,false);
        }

        TextView firstNameTxt= (TextView) view.findViewById(R.id.firstNameTxt);
        TextView lastNameTxt= (TextView) view.findViewById(R.id.lastNameTxt);
        TextView emailTxt= (TextView) view.findViewById(R.id.emailTxt);
        TextView phoneTxt= (TextView) view.findViewById(R.id.phoneTxt);

        User user= (User) this.getItem(i);

        final String firstName=user.getFirstName();
        final String lastName=user.getLastName();
        final String email=user.getEmail();
        final String phone =user.getPhone();
        final String photo =user.getPhoto();

        firstNameTxt.setText(firstName);
        lastNameTxt.setText(lastName);
        emailTxt.setText(email);
        phoneTxt.setText(phone);




        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //OPEN DETAIL ACTIVITY
               openDetailActivity(firstName,lastName,email,phone,photo);

            }
        });
        return view;
    }
    ////open activity
    private void openDetailActivity(String...details)
    {
        Intent i=new Intent(c,DetailActivity.class);
        i.putExtra("FIRST_NAME_KEY",details[0]);
        i.putExtra("LAST_NAME_KEY",details[1]);
        i.putExtra("EMAIL_KEY",details[2]);
        i.putExtra("PHONE_KEY",details[3]);
        i.putExtra("PHOTO_KEY", details[4]);

        c.startActivity(i);

    }

}













